seguir = True
while seguir:
    try:
        print("Dame un número entero no negativo: ")
        numero1 = int(input())
        print("Dame otro: ")
        numero2 = int(input())
        suma = 0

        if numero1 < 0 or numero2 < 0:
            print("Los números deben ser positivos")
        else:
            if numero1 > numero2:
                numero1, numero2 = numero2, numero1

            if numero1 % 2 != 0:
                primer_impar = numero1
            else:
                primer_impar = numero1 + 1

            for impares in range(primer_impar, numero2 + 1, 2):
                suma = suma + impares

            print("La suma de los números impares comprendidos entre los dados es: ", suma)
            seguir = False

    except:
        print("Algo no ha ido como debía")
